package com.atguigu.gmallpublisher.service;

import java.math.BigDecimal;

public interface GmvService {

    BigDecimal getGmv(int date);

}
