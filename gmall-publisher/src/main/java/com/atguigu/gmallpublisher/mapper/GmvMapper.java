package com.atguigu.gmallpublisher.mapper;

import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;

public interface GmvMapper {

    @Select("select sum(order_amount) order_amount from dws_trade_user_spu_order_window where toYYYYMMDD(stt)=#{date}")
    BigDecimal selectGmv(int date);

}
